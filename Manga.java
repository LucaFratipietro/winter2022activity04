public class Manga{
	
	//Fields 
	
	private String title;
	private int retailPrice;
	private String publishYear;
	
	
	//Get Set Methods
	
	public String getTitle(){
		return this.title;
	}
	
	//Missing setTitle

	public int getRetailPrice(){
		return this.retailPrice;
	}
	
	public void setRetailPrice(int price){
		this.retailPrice = price;
	}
	
	public String getPublishYear(){
		return this.publishYear;
	}
	
	public void setPublishYear(String year){
		this.publishYear = year;
	}
	
	//Constructor
	
	public Manga(String title, int retailPrice, String publishYear){
		this.title = title;
		this.retailPrice = retailPrice;
		this.publishYear = publishYear;
	}
	
	public void howExpensive(){
		if (this.retailPrice >= 20){
			System.out.println("That one is pricy!");
		}
		else {
			System.out.println("That one is pretty cheap!");
		}
	}
}