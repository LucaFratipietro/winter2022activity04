import java.util.Scanner;

public class Shop{
	public static void main (String [] args){
		
		Scanner reader = new Scanner (System.in);
		
		Manga[] productList = new Manga[4];
		for (int i=0; i < productList.length; i++){
			//productList[i] = new Manga();
			
			System.out.println("What is the title of the manga?");
			String newTitle = reader.nextLine();
			//productList[i].setTitle(reader.nextLine());
			
			System.out.println("What is the retail price?");
			int retailPrice = Integer.parseInt(reader.nextLine());
			//productList[i].setRetailPrice(retailPrice);
			
			System.out.println("What is the publising year?");
			String publishYear = reader.nextLine();
			//productList[i].setPublishYear(publishYear);
			
			productList[i] = new Manga(newTitle,retailPrice,publishYear);

		}
		
		System.out.println("Before the call:");
		System.out.println(productList[3].getTitle());
		System.out.println(productList[3].getRetailPrice());
		System.out.println(productList[3].getPublishYear());
		
		productList[3].howExpensive();
		
		System.out.println("setTitle was deleted so changing the title field is now impossible");
		System.out.println("What is the new retail price of the last product?");
		productList[3].setRetailPrice(Integer.parseInt(reader.nextLine()));
		System.out.println("What is the new publshing year of the last product?");
		productList[3].setPublishYear(reader.nextLine());
		
		System.out.println("After the call:");
		System.out.println(productList[3].getTitle());
		System.out.println(productList[3].getRetailPrice());
		System.out.println(productList[3].getPublishYear());
		
		productList[3].howExpensive();
	}
}